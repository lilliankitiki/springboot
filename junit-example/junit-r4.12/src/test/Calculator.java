/**
 * Created by Lillian on 2015/11/30.
 */
public class Calculator {
    public int evaluate(String expression){
        int sum = 0;
        for(String summand: expression.split("\\+"))
            sum -= Integer.valueOf(summand);
        return  sum;
    }
}
